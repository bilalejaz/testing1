'use strict';

//require('./core.scss');

var name = module.exports = 'BolSelfie.core';

// Fix to make restangular work with webpack since it doesn't support common js.
// For more info: https://github.com/mgonto/restangular/issues/749
require('restangular');
require('angular-local-storage');
require('angular-moment');
require('angular-material');


require('angular-drag-and-drop-lists');
require("jquery");
//require('nms-modules');
require('ng-file-upload');
require('angular-toastr');

angular
  .module(name, [
    require('angular-ui-router'),
    'restangular',
    'LocalStorageModule',
    'angularMoment',
    'ngMaterial',
    'ui.select2',
    'ngScrollbars',
    'daterangepicker',
    'ngFileUpload',
    'dndLists',
    'ngAnimate',
    'toastr'
    //,
   // 'nms-modules'
  ])
    //.constant('$', require("jquery"))
    .value('lodash', _)
    .config(require('./restangular.config.js'))
    .config(require('./router.config.js'))
    .config(function (ScrollBarsProvider) {
        ScrollBarsProvider.defaults = {
            axis: 'y', // enable 2 axis scrollbars by default
            autoHideScrollbar: false,
            theme: 'minimal',
            advanced: {
                updateOnContentResize: true
            },
        };
    })
    //.directive('dateRangePick', function () {
    //    return {
    //        restrict: 'E',
    //        templateUrl: 'assets/directives/_dateRangePicker.html'
    //    };
    //}).directive('dateSinglePick', function () {
    //    return {
    //        restrict: 'E',
    //        templateUrl: 'assets/directives/_dateSinglePicker.html'
    //    };
    //}).directive('dateSingleTimePick', function () {
    //    return {
    //        restrict: 'E',
    //        templateUrl: 'assets/directives/_dateSingleTimePicker.html'
    //    };
    //})
    .run(require('./router.run.js'))
    .run(function ($rootScope) {
        $rootScope.safeApply = function (fn) {
            var phase = this.$root.$$phase;
            if (phase == '$apply' || phase == '$digest') {
                if (fn && (typeof (fn) === 'function')) {
                    fn();
                }
            } else {
                this.$apply(fn);
            }
        };
    }).filter('groupBy', function ($timeout) {
        return function (data, key) {
            if (!key) return data;
            var outputPropertyName = '__groupBy__' + key;
            if(!data[outputPropertyName]){
                var result = {};  
                for (var i=0;i<data.length;i++) {
                    if (!result[data[i][key]])
                        result[data[i][key]]=[];
                    result[data[i][key]].push(data[i]);
                }
                Object.defineProperty(data, outputPropertyName, {enumerable:false, configurable:true, writable: false, value:result});
                $timeout(function(){delete data[outputPropertyName];},0,false);
            }
            return data[outputPropertyName];
        };
    })
;







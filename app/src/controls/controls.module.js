'use strict';

//require('./controls.scss');

var name = module.exports = 'BolSelfie.controls';

angular
  .module(name, [])
  .config(configuration)
  .controller('Controls', require('./controls.controller.js'))
  .controller('Storypopup', require('./storypopup.controller.js'))
;

function configuration($stateProvider) {
  $stateProvider
    .state('controls', {
      url: '/controls',
      template: require('./controls.html'),
      controller: 'Controls as vm',
      title: 'Welcome to Wayne Minor.'
    });
}

'use strict';

module.exports = factory;

/* @ngInject */
function factory(Restangular, localStorageService) {

    return {
        login: login,
        LoadInitialData: LoadInitialData,
        loadInitialData: loadInitialData,
        ChangePassword: ChangePassword,
        GetAllBroadcastedChannelStories: GetAllBroadcastedChannelStories,
        GetAllPrograms: GetAllPrograms,
        UpsertBroadcastedEpisode: UpsertBroadcastedEpisode,
        GetBroadcastedEpisodeDetailById: GetBroadcastedEpisodeDetailById,
        GetAllSelfies: GetAllSelfies,
        MarkSelfiesApproval: MarkSelfiesApproval,
        MarkContestantsApproved:MarkContestantsApproved,
        GetAllSelfiesGroupByUser: GetAllSelfiesGroupByUser,
        GetAllSelfiesByUserName: GetAllSelfiesByUserName,
        GetAllGiftType: GetAllGiftType,
        GetAllGameType: GetAllGameType,
        getPrizeType: getPrizeType,
        getGameType: getGameType,
        GetAllWinners: GetAllWinners,
        GetApiSearch: GetApiSearch,
        ProcessBalloting: ProcessBalloting,
        GetWhatsAppMsgs: GetWhatsAppMsgs,
        PostEditForm: PostEditForm,
        SelfieReport: SelfieReport,
        LoadWinnersData: LoadWinnersData,
        InsertWinner: InsertWinner,
        GetQuizResults: GetQuizResults,
        GetAnswerDetailsForBalloting: GetAnswerDetailsForBalloting,
        GetCorrectAnserDetail: GetCorrectAnserDetail,
        GetBallotedUsers: GetBallotedUsers,
        GetAllContestantData: GetAllContestantData
    };

    function login(username, password) {
        var data = { Login: username, Password: password };
        return Restangular.all('user/LoginMMS/').post(data);
    }

    function ChangePassword(obj) {
        var url = 'user/ChangePassword?userId=' + obj.userId + '&password=' + obj.password;
        return Restangular.one(url).get();
    }

    function LoadInitialData(userId, userRole) {

        var url = 'News/GetAllUsers';
        var requestObj = {
            UserId: userId,
            UserRole: userRole
        };
        Restangular.all(url).post(requestObj).then(function (data) {
            if (data.IsSuccess) {
                localStorageService.set('allUsers', data.Data);
            }
        });

    }

    function loadInitialData(userId, userRole) {
        var previousDate = new Date();
        previousDate = previousDate.setDate(previousDate.getDate() - 1);
        var data = {
            UserId: userId,
            UserRole: userRole,
            FromStr: (new Date(previousDate)).toISOString(),
            ToStr: (new Date()).toISOString(),
            FiltersToDiscard: [{ FilterTypeId: 3, FilterId: 25 }, { FilterTypeId: 3, FilterId: 26 }, { FilterTypeId: 13, FilterId: 76 }],
            Filters: [{ FilterTypeId: 16, FilterId: 78 }]
        };
        return Restangular.all('news/LoadInitialDataNMS').post(data);
    }

    function GetAllBroadcastedChannelStories(date)
    {
        var url = 'BroadcastedEpisode/GetAllBroadcastedChannelStories?From='+date;
        return Restangular.one(url).get();
    }

    function GetAllPrograms() {
        var url = 'program/GetAll';
        return Restangular.one(url).get();
    }

    function UpsertBroadcastedEpisode(obj) {
        var url = 'BroadcastedEpisode/UpsertBroadcastedEpisode';
        return Restangular.all(url).post(obj);
    }

    function GetBroadcastedEpisodeDetailById(BroadcastedEpisodeId) {
        var url = 'BroadcastedEpisode/GetBroadcastedEpisodeDetailById?BroadcastedEpisodeId=' + BroadcastedEpisodeId;
        return Restangular.one(url).get();
    }

    function GetAllSelfies(startDate, EndDate) {
        var url = 'socialmediahashtag/get?fromStr=' + startDate + '&toStr=' + EndDate;
        return Restangular.one(url).get();
    }
    function GetAllContestantData(startDate, EndDate) {
        var url = 'socialmediahashtag/GetAllContestantData?fromStr=' + startDate + '&toStr=' + EndDate;
        return Restangular.one(url).get();
    }
    
    function MarkSelfiesApproval(obj) {
        var url = 'socialmediahashtag/MarkSelfiesApproval';
        return Restangular.all(url).post(obj);
    }
    function MarkContestantsApproved(obj) {
        var url = 'socialmediahashtag/MarkContestantApproved';
        return Restangular.all(url).post(obj);
    }
    
    function GetAllSelfiesGroupByUser(stDate, enDate, FilterType) {
        var url = 'socialmediahashtag/LoadAllSelfiesByUser?fromStr=' + stDate + '&toStr=' + enDate + '&FilterType=' + FilterType;
        return Restangular.one(url).get();
    }
    function GetAllSelfiesByUserName(userId) {
        var url = 'socialmediahashtag/GetSelfiesListByUserId?UserID=' + userId;
        return Restangular.one(url).get();
    }

    function GetAllGiftType() {
        var url = 'socialmediahashtag/GetAllGiftType/get';
        return Restangular.one(url).get();
    }
    function GetAllGameType() {
        var url = 'socialmediahashtag/GetAllGameType/get';
        return Restangular.one(url).get();
    }

    //winners screen 
    function getPrizeType() {
        var url = 'socialmediahashtag/GetAllGiftType';
        return Restangular.one(url).get();
    }


    function getGameType() {
        var url = 'socialmediahashtag/GetAllGameType';
        return Restangular.one(url).get();
    }

    function GetAllWinners(postData) {
        var url = 'socialmediahashtag/LoadAllSelfiesByUser';
        return Restangular.all(url).post(postData);
    }

    function GetApiSearch() {
        var url = 'SocialMediahashTag/Get?fromStr=2017-04-06&toStr=2017-04-06';
        return Restangular.one(url).get();
    }
    function ProcessBalloting(GiftType, GameType, Location, StartDate, EndData, SelfieCount, BallotedCount) {
        var url = 'socialmediahashtag/ProcessBalloting?gifttype=' + GiftType + '&gametype=' + GameType + '&location=' + Location + '&startdate=' + StartDate + '&enddate=' + EndData + '&selfiecount=' + SelfieCount + '&ballotedcount=' + BallotedCount;
        return Restangular.one(url).get();
    }
    //WhatsApp TextMsgs

    function GetWhatsAppMsgs(postData) {
        var url = 'socialmediahashtag//LoadWhatsappData?from=' + postData.from+"&to="+postData.to;
        return Restangular.one(url).get();
    }

    function PostEditForm(postData) {
        var url = 'socialmediahashtag/SaveWhatsAppUserReply';
        return Restangular.all(url).post(postData);
    }
    function LoadWinnersData(postData) {
        var url = 'socialmediahashtag/LoadWinnersData';
        return Restangular.all(url).post(postData);
    }
    function SelfieReport(postData) {
        var url = 'socialmediahashtag//SelfieReport?from=' + postData.from + "&to=" + postData.to;
        return Restangular.one(url).get();
    }
    function InsertWinner(postData) {
        var url = 'socialmediahashtag/InsertWinner';
        return Restangular.all(url).post(postData);
    }

    function GetQuizResults(Date, StartTime, EndTime, GameType) {
        var url = 'SocialMediahashTag/GetAllAnswers?date=' + Date + '&startTime=' + StartTime + '&endTime=' + EndTime + '&gameType=' + GameType + '';
        return Restangular.one(url).get();
    }

    function GetAnswerDetailsForBalloting(QuestionNumber, Answer, Date, StartTime, EndTime, GameType) {
        var url = 'SocialMediahashTag/GetCorrectAnserDetail?questionNo=' + QuestionNumber + '&Answer=' + Answer + '&date=' + Date + '&startTime=' + StartTime + '&endTime=' + EndTime + '&gameType=' + GameType + ' ';
        return Restangular.one(url).get();
    }

    function GetCorrectAnserDetail(QuestionNumber, Answer, Date, StartTime, EndTime, GameType, ballotedCount) {
        var url = 'SocialMediahashTag/GetCorrectAnserDetail?questionNo=' + QuestionNumber + '&Answer=' + Answer + '&date=' + Date + '&startTime=' + StartTime + '&endTime=' + EndTime + '&gameType=' + GameType + '&ballotedCount=' + ballotedCount + ' ';
        return Restangular.one(url).get();
    }

    function GetBallotedUsers(QuestionNumber, Answer, Date, StartTime, EndTime, GameType, ballotedCount, ShowUsers) {
        var url = 'SocialMediahashTag/GetCorrectAnserDetail?questionNo=' + QuestionNumber + '&Answer=' + Answer + '&date=' + Date + '&startTime=' + StartTime + '&endTime=' + EndTime + '&gameType=' + GameType + '&ballotedCount=' + ballotedCount + '&ShowUsers=' + ShowUsers + ' ';
        return Restangular.one(url).get();
    }
}
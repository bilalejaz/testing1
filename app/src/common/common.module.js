'use strict';

//require('./common.scss');

var name = module.exports = 'BolSelfie.common';

angular.module(name, [])
  .factory('userservice', require('./userservice.factory.js'))
  .factory('dataservice', require('./dataservice.factory.js'))
  .factory('rabbitmq', require('./rabbitmq.factory.js'))
  .constant('Config', require('./config.constant.js'));

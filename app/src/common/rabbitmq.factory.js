'use strict';

module.exports = factory;

/* @ngInject */
function factory(Config) {
  
    var client, isConnected;

    function connect() {
        var ws = new SockJS(Config.RabbitMQServerAddress + '/stomp');
        client = Stomp.over(ws);
        client.heartbeat.outgoing = 10000;
        client.heartbeat.incoming = 0;
        client.connect(Config.RabbitMQUserName, Config.RabbitMQPassword, onConnect, onError, '/');
    }

    function onConnect() {
        isConnected = true;
        console.log('RabitMQ Connected');
    }

    function onError(err) {
        isConnected = false;
        console.log('RabitMQ Error', err);
        setTimeout(function () { connect(); }, 5000);
    }
    function init() {
        connect();
    }

    function unsubscribe(path) {
        if (isConnected) {
            client.unsubscribe("/exchange/" + path);
        } else {
            setTimeout(function () {
                unsubscribe(path);
            }, 2000);
        }
    }
    function subscribe(path, callback) {
        if (isConnected) {
            var subId = client.subscribe("/exchange/" + path, function (d) {
                if (callback) {
                    callback(JSON.parse(d.body));
                }
            });
        } else {
            setTimeout(function () {
                subscribe(path, callback);
            }, 2000);
        }
    }
    init();

    return {
        subscribe: subscribe,
        unsubscribe: unsubscribe
    };
}

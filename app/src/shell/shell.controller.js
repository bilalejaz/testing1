'use strict';

module.exports = controller;

/* @ngInject */
function controller($log, $scope, userservice, $state, dataservice, toastr) {
    var vm = this;

  vm.welcomeMessage = 'Yet another generator for angular powered by webpack.';
  vm.menuItems = [
      //{ text: 'Selfie Verification', route: 'shell.bolselfieverification' },
      { text: 'Selfie Verification', route: 'shell.contestants' },
      { text: 'Users Selfie', route: 'shell.home' },
      { text: 'Balloting', route: 'shell.balloting' },
      { text: 'Winners', route: 'shell.winnerScreen' },
      { text: 'WhatsAppText', route: 'shell.WhatsappTextMsgs' },
      { text: 'StatusReport', route: 'shell.statusreport' },
      { text: 'Selfie Game Balloting', route: 'shell.selfiegameballoting' }
      ];
  
  //vm.menuItems.push({ text: 'Add Episode', route: 'shell.addbroadcastedepisode' });

    vm.logout = function () {
        userservice.logout();
    }

    //vm.changePassword = function () {
    //    $(".pass_popup").css('display','initial');
    //}

    vm.showScrollPop = function () {
        //if ($('.ssspop').css('display', 'initial') == true) {

        //}
        $('.ssspop').css('display', 'initial');
    }

    vm.changePassword = function () {
        vm.showChangePassword = true;
        $(".pass_popup").css('display', 'initial');
    }
    vm.closePasswordPopup = function () {
        $(".pass_popup").css('display', 'none');
    }
    vm.submitPassword = function () {
        debugger;
        if (vm.newPass == undefined) {
            toastr.warning('Please Enter Password');
            return false;
        }
        if (vm.confrmPass == undefined || vm.confrmPass == '') {
            toastr.warning('Please Re-Enter Password');
            return false;
        }
        if (vm.newPass != '' && (vm.newPass == vm.confrmPass)) {
            var reqObj = {
                userId: userservice.getUserId(), password: vm.newPass
            }
            dataservice.ChangePassword(reqObj).then(function (data) {
                if (data.IsSuccess) {
                    toastr.info('Password Change Successfully...');
                    toastr.info('Kindly Login With New Credentials...');
                    setTimeout(function () {
                        userservice.logout();
                    }, 2000);
                }
            });
        }
    }


    $(document).mouseup(function (e) {
        var container = $(".ssspop");

        if (!container.is(e.target)
            && container.has(e.target).length === 0)
        {
            container.hide();
        }
    });

}

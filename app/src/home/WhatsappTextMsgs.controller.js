'use strict';

module.exports = controller;

/* @ngInject */
function controller($log, Restangular, dataservice, $scope, $rootScope, toastr) {
  var vm = this;


  vm.startDate = "";
  vm.endDate = "";
  vm.stDate = "";
  vm.enDate = "";
  $scope.myVar = false;


  vm.onDateRangeChange = function (startDate, endDate) {
    vm.startDate = startDate;
    vm.endDate = endDate;
    vm.WhatsAppData = [];
    vm.Apply(startDate, endDate);
  }

  //Apply Button
  vm.Apply = function (startDate, endDate) {
      var dd = startDate.getDate();
      var mm = startDate.getMonth() + 1;
      var yyyy = startDate.getFullYear();
      var stdate = yyyy + '/' + mm + '/' + dd;
      vm.stDate = stdate.toString();

      var edd = endDate.getDate();
      var emm = endDate.getMonth() + 1;
      var eyyyy = endDate.getFullYear();
      var endate = eyyyy + '/' + emm + '/' + edd;
      vm.enDate = endate.toString();

    var postData = {
        from: vm.stDate,
        to: vm.enDate,
    };
    dataservice.GetWhatsAppMsgs(postData).then(function (data) {
      var WhatsAppData = JSON.parse(data);
      for (i = 0; i < WhatsAppData.length; i++) {
          WhatsAppData[i].myVar = false;
          WhatsAppData[i].detail = {};
      }
      vm.WhatsAppData = WhatsAppData;
      //vm.WhatsAppData = WhatsAppData.map((data) => {
      //    data.detail = {};
      //    return data;
      //});

    });

  };


  //Edit Form
  vm.PostEditForm = function (detail,contact) {
    var postData = {
      Name: detail.Name,
      Email: detail.Email,
      Address: detail.Address,
      Age: detail.Age,
      FbProfile: detail.fbProfile,
      TwitterProfile: detail.twitterProfile,
      ContactNo: contact,

    };
    dataservice.PostEditForm(postData).then(function (WhatsAppMsgs) {

    });

  };



  vm.hide = function (WhatsAppData) {
    WhatsAppData.myVar = true;
  }

  vm.hideForm = function (WhatsAppData) {

    WhatsAppData.myVar = false;

  }



}

﻿'use strict';

module.exports = controller;
/* @ngInject */
function controller($log, Restangular, dataservice, toastr, $state, $location, $timeout, rabbitmq, $scope, Config, $rootScope) {

    var vm = this;
    vm.Date = '';
    vm.Loader = false;
    vm.stDate = '';
    vm.enDate = '';
    vm.ShowData = false;
    vm.CurrentGameType = 0;
    vm.IsQuizGame = false;
    vm.IsFirstBalloting = false;
    vm.IsSecondBalloting = false;
    vm.IsFinalUsers = false;
    vm.QuizResults = [];
    vm.FirstBallotingResults = [];
    vm.FinalBallotingResults = [];
    vm.BallotedUsersList = [];
    vm.startTime = '';
    vm.endTime = '';


    vm.GameTypeOptions = [{ name: 'Select Game', value: 0 }];

    function Init() {
        $('input[type="time"][value="starttime"]').each(function () {
            var d = new Date(),
                h = '00',
                m = '00';
            $(this).attr({
                'value': h + ':' + m
            });
        });

        $('input[type="time"][value="endtime"]').each(function () {
            var d = new Date(),
                h = d.getHours(),
                m = d.getMinutes();
            if (h < 10) h = '0' + h;
            if (m < 10) m = '0' + m;
            vm.EndTime = h + ':' + m;
            $(this).attr({
                'value': h + ':' + m
            });
        });

        dataservice.GetAllGameType().then(function (data) {
            if (data && data.length > 0) {
                for (var m = 0; m <= data.length - 1; m++) {
                    vm.GameTypeOptions.push({ name: data[m].name, value: data[m].id });
                }
            }
        });
    }

    vm.onDateSelect = function (NewDate) {
        vm.Date = '';
        var dd = NewDate.getDate();
        var mm = NewDate.getMonth() + 1;
        var yyyy = NewDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.Date = stdate.toString();
        console.log(vm.Date);
    }

    vm.onTimeChange = function (NewTime) {
        debugger;
        console.log(NewTime);
    }

    vm.GameTypeSelection = function (val) {
        vm.CurrentGameType = val;
    }

    vm.StartBalloting = function () {
        vm.IsQuizGame = false;
        vm.IsFirstBalloting = false;
        vm.IsSecondBalloting = false;
        vm.IsFinalUsers = false;
        var startTime = $('.st_time').val();
        var endTime = $('.en_time').val();
        vm.startTime = startTime;
        vm.endTime = endTime;

        if (startTime == '') {
            return toastr.warning('Please Enter Start Time');
        }
        if (endTime == '') {
            return toastr.warning('Please Enter End Time');
        }

        if (startTime != '' && endTime != '') {
            var s = startTime.split(":");
            var e = endTime.split(":");
            var a = parseInt(s[0]);
            var b = parseInt(s[1]);
            var c = parseInt(e[0]);
            var d = parseInt(e[1]);

            if (a > c) {
                return toastr.warning('Please Enter End Time');
            }
            if (a == c && b > d) {
                return toastr.warning('Please Enter End Time');
            }
        }

        if (vm.CurrentGameType > 0) {
            if (vm.CurrentGameType == 3) {
                dataservice.GetQuizResults(vm.Date, startTime, endTime, vm.CurrentGameType).then(function (data) {
                    if (data && data.length > 0) {
                        vm.QuizResults = data;
                        vm.IsQuizGame = true;

                        setTimeout(function () {
                            $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight;
                        }, 1000);

                    }
                });
            }
            if (vm.CurrentGameType == 4) {
                vm.IsFirstBalloting = true;
            }
        }
        else {
            return toastr.warning('Please Select Game Type');
        }

    }

    vm.ViewGameBalloting = function (answer, questionNumber) {
        vm.FirstBallotingResults = [];
        vm.Question = questionNumber;
        vm.Answer = answer.option;
        dataservice.GetAnswerDetailsForBalloting(questionNumber, answer.option, vm.Date, vm.startTime, vm.endTime, vm.CurrentGameType).then(function (data) {
            if (data.cityViseCout && data.cityViseCout.length > 0) {
                vm.IsFirstBalloting = true;
                vm.FirstBallotingResults = data;
                setTimeout(function () { $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight; }, 1000);
            }
            else {
                return toastr.warning('No Record!');
            }
        });
    }

    vm.SubmitFirstBallot = function () {
        vm.FinalBallotingResults = [];
        if (vm.FirstBallotCount) {
            dataservice.GetCorrectAnserDetail(vm.Question, vm.Answer, vm.Date, vm.startTime, vm.endTime, vm.CurrentGameType, vm.FirstBallotCount).then(function (data) {
                if (data.cityViseCout && data.cityViseCout.length > 0) {
                    vm.IsSecondBalloting = true;
                    vm.FinalBallotingResults = data;
                    setTimeout(function () { $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight; }, 1000);
                }
            });
        }
        else {
            return toastr.warning('Please Enter Balloted Count!');
        }
    }

    vm.GetBallotedUsers = function () {
        vm.BallotedUsersList = [];
        if (vm.FinalBallotCount) {
            dataservice.GetBallotedUsers(vm.Question, vm.Answer, vm.Date, vm.startTime, vm.endTime, vm.CurrentGameType, vm.FinalBallotCount, true).then(function (data) {
                if (data.contestants && data.contestants.length > 0) {
                    for (var i = 0; i <= data.contestants.length - 1; i++) {
                        data.contestants[i].mediaSourceName = data.contestants[i].mediaSourceType;
                    }

                    vm.BallotedUsersList = data.contestants;
                    vm.IsFinalUsers = true;

                    setTimeout(function () { $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight; }, 1000);
                }
            });
        }
        else {
            return toastr.warning('Please Enter Balloted Count!');
        }
    }


    Init();
}
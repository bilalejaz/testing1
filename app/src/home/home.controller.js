﻿//'use strict';

module.exports = controller;

/* @ngInject */
function controller($state, $scope, $window, $timeout, $stateParams, dataservice, userservice, Config, toastr, $rootScope) {
    var vm = this;
    vm.Loader = true;
    vm.selfies_Users = [];
    vm.selfiesOfUser = [];
    vm.showImageViewer = false;
    vm.stDate = '';
    vm.enDate = '';
    vm.FilterType = 0;
    vm.IsLoaded = false;
    vm.currentIndex = 0;
    vm.OgList = [];
    vm.finalselfiesOfUser = [];
    vm.usersCount = 0;

    vm.socialmediaOptions =
        [
            { name: 'All Type', value: 0 },
            { name: 'Email', value: 3 },
            { name: 'Facebook', value: 2 },
            { name: 'Twitter', value: 1 },
            { name: 'WhatsApp', value: 4 }
        ];

    function init() {

    }

    vm.onDateRangeChange = function (startDate, endDate) {
        vm.selfies_Users = [];
        vm.Date = '';
        vm.Loader = true;
        var dd = startDate.getDate();
        var mm = startDate.getMonth() + 1;
        var yyyy = startDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.stDate = stdate.toString();

        var edd = endDate.getDate();
        var emm = endDate.getMonth() + 1;
        var eyyyy = endDate.getFullYear();
        var endate = eyyyy + '/' + emm + '/' + edd;
        vm.enDate = endate.toString();

        dataservice.GetAllSelfiesGroupByUser(vm.stDate, vm.enDate, vm.FilterType).then(function (data) {
            if (data && data.length > 0) {
                var temp = [];
                for (var i = 0; i <= data.length - 1; i++) {
                    if (data[i].item.mediaSourceType == 1) {
                        data[i].item.mediaSourceName = 'Twitter';
                    }
                        if (data[i].item.mediaSourceType == 2) {
                            data[i].item.mediaSourceName = 'Facebook';
                        }
                    if (data[i].item.mediaSourceType == 3) {
                        data[i].item.mediaSourceName = 'Email';
                    }          
                    if (data[i].item.mediaSourceType == 4) {
                        data[i].item.mediaSourceName = 'WhatsApp';
                    }


                    if (i < 50) {
                        temp.push(data[i]);
                    }
                }

                vm.currentIndex = 50;
                vm.selfies_Users = temp;
                vm.OgList = data;
                //vm.selfies_Users = data;
                vm.Loader = false;
                vm.IsLoaded = true;
            }
            else {
                vm.selfies_Users = [];
                vm.Loader = false;
                vm.IsLoaded = true;
            }
        });
    }


    vm.onScrollWindow = function () {
        var k = 0;
        var ptemp = [];
        for (var i = vm.currentIndex; i <= vm.OgList.length - 1; i++) {
            if (k < 50) {
                ptemp.push(vm.OgList[i]);
            }
            else
                break;

            k = k + 1;
        }
        $rootScope.safeApply(function () {
            vm.selfies_Users = vm.selfies_Users.concat(ptemp);
        });
        vm.currentIndex = vm.currentIndex + k;
    }

    $('.ScrollCss').scroll(function () {
        if ($('.ScrollCss').scrollTop() + $('.ScrollCss').height() >= $('.BodyScroll').height()) {
            vm.onScrollWindow();
        }
    });

    vm.SocialMediaSelection = function (item) {
        vm.selfies_Users = [];
        vm.OgList = [];
        vm.currentIndex = 0;
        vm.FilterType = item;
        vm.Loader = true;
        dataservice.GetAllSelfiesGroupByUser(vm.stDate, vm.enDate, vm.FilterType).then(function (data) {
            if (data && data.length > 0) {
                var temp = [];
                for (var i = 0; i <= data.length - 1; i++) {
                    if (data[i].item.mediaSourceType == 1) {
                        data[i].item.mediaSourceName = 'Twitter';
                    }
                    if (data[i].item.mediaSourceType == 2) {
                        data[i].item.mediaSourceName = 'Facebook';
                    }
                    if (data[i].item.mediaSourceType == 3) {
                        data[i].item.mediaSourceName = 'Email';
                    }
                    if (data[i].item.mediaSourceType == 4) {
                        data[i].item.mediaSourceName = 'WhatsApp';
                    }


                    if (i < 50) {
                        temp.push(data[i]);
                    }
                }

                vm.currentIndex = 50;
                vm.selfies_Users = temp;
                vm.OgList = data;
                vm.Loader = false;
                vm.IsLoaded = true;
            }
            else {
                vm.selfies_Users = [];
                vm.Loader = false;
                vm.IsLoaded = true;
            }
        });
    }


    init();
    vm.closeIframe = function () {
        $(".pass_popup1").css('display', 'none');
        vm.showImageViewer = false;
        vm.selfiesOfUser = [];
    }

    vm.showDetail = function (user) {
        $(".pass_popup1").css('display', 'initial');
        vm.showImageViewer = true;
        var userName = user.name;
        dataservice.GetAllSelfiesByUserName(userName).then(function (data) {
            if (data && data.length > 0) {
                vm.selfiesOfUser = data;
            }
        });
    }
}

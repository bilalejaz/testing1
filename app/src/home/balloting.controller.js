﻿'use strict';

module.exports = controller;
/* @ngInject */
function controller($log, Restangular, dataservice, toastr, $state, $location, $timeout, rabbitmq, $scope, Config, $rootScope) {

    var vm = this;
    vm.currDate = '';
    vm.BroadcastedProgramData = [];
    vm.Loader = false;
    vm.IsIconChecked = false;
    vm.AllSelfieIds = [];
    vm.OgList = [];
    vm.finalSelfies = [];
    vm.currentIndex = 0;
    vm.AllSelfies = [];
    vm.stDate = '';
    vm.enDate = '';
    vm.CurrentGiftType = 0;
    vm.CurrentGameType = 0;
    vm.LocationId = 0;
    vm.CurrentSelfieCount = 0;
    vm.BallotedData = [];
    vm.ShowData = false;

    vm.GiftTypeOptions = [{ name: 'Select Gift', value: 0 }];
    vm.GameTypeOptions = [{ name: 'Select Game', value: 0 }];
    vm.SelfieCountOptions = [{ name: 'Select Selfie Count', value: 0 }, { name: '1', value: 1 }, { name: '2', value: 2 }, { name: '3', value: 3 }, { name: '4', value: 4 }, { name: '5', value: 5 }, { name: '6', value: 6 }, { name: '7', value: 7 }, { name: '8', value: 8 }, { name: '9', value: 9 }, { name: '10', value: 10 }];

    function Init() {

        dataservice.GetAllGiftType().then(function (data) {
            if (data && data.length > 0) {
                for (var i = 0; i <= data.length - 1; i++) {
                    vm.GiftTypeOptions.push({ name: data[i].name, value: data[i].id });
                }
            }
        });

        dataservice.GetAllGameType().then(function (data) {
            if (data && data.length > 0) {
                for (var m = 0; m <= data.length - m; m++) {
                    vm.GameTypeOptions.push({ name: data[m].name, value: data[m].id });
                }
            }
        });
    }

    vm.onDateRangeChange = function (startDate, endDate) {
        vm.Date = '';
        var dd = startDate.getDate();
        var mm = startDate.getMonth() + 1;
        var yyyy = startDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.stDate = stdate.toString();

        var edd = endDate.getDate();
        var emm = endDate.getMonth() + 1;
        var eyyyy = endDate.getFullYear();
        var endate = eyyyy + '/' + emm + '/' + edd;
        vm.enDate = endate.toString();
    }

    vm.GiftTypeSelection = function (val) {
        vm.CurrentGiftType = val;
    }

    vm.GameTypeSelection = function (val) {
        vm.CurrentGameType = val;
    }

    vm.SelfieCountSelection = function (val) {
        vm.CurrentSelfieCount = val;
    }

    vm.onSelectLocation = function (locationId) {
        vm.LocationId = locationId;
    }

    vm.StartBalloting = function (GiftType,GameType) {
        //var reqObj = {
        //    GiftType: vm.CurrentGiftType,
        //    GameType: vm.CurrentGameType,
        //    Location: vm.LocationId,
        //    StartDate: vm.stDate,
        //    EndData: vm.enDate,
        //    SelfieCount: vm.CurrentSelfieCount,
        //    BallotedCount: vm.BallotedCount
        //};
        
        if (GiftType == 0) {
            toastr.warning("Please Select Gift Type!");
        }

        else if (GameType == 0) {

            toastr.warning("Please Select Game Type!");

        }
        else 
            {

            vm.ShowData = true;
        vm.BallotedData = [];
        vm.Loader = true;
        if (!vm.BallotedCount) {
            vm.BallotedCount = '';
        }

        dataservice.ProcessBalloting(vm.CurrentGiftType, vm.CurrentGameType, vm.LocationId, vm.stDate, vm.enDate, vm.CurrentSelfieCount, vm.BallotedCount).then(function (data) {
            if (data.length > 0) {
                var Data = JSON.parse(data);
                if (Data.length > 0) {
                    for (var i = 0; i <= Data.length - 1; i++) {
                        if (Data[i].MediaSourceType == '1') {
                            Data[i].MediaSourceName = 'Twitter';
                        }
                        if (Data[i].MediaSourceType == '2') {
                            Data[i].MediaSourceName = 'Facebook';
                        }
                        if (Data[i].MediaSourceType == '3') {
                            Data[i].MediaSourceName = 'WhatsApp';
                        }
                        if (Data[i].MediaSourceType == '4') {
                            Data[i].MediaSourceName = 'Email';
                        }

                        Data[i].IsWinner = false;
                        Data[i].IsTrueWinner = false;
                    }

                    vm.BallotedData = Data;
                    vm.Loader = false;
                }
                else {
                    vm.Loader = false;
                }
            }
            else {
                vm.Loader = false;
            }
        });
    }
    }

    vm.onSelectWinner = function (data, chkValue, AllData) {
        for (var n = 0; n <= AllData.length - 1; n++) {
            AllData[n].IsWinner = false;
            AllData[n].IsTrueWinner = false;
        }

        if (chkValue) {
            data.IsWinner = chkValue;
            data.IsTrueWinner = true;
        }
    }

    vm.ApproveWinner = function (data) {
        if (vm.CurrentGiftType == 0) {
            return toastr.warning("Please Select Gift Type!");
        }
        if (vm.CurrentGameType == 0) {
            return toastr.warning("Please Select Game Type!");
        }
        if (!vm.CurrentSelfieCount && vm.CurrentSelfieCount == 0) {
            return toastr.warning("Please Select Selfie Count!");
        }
        if (!vm.BallotedCount && vm.BallotedCount == 0) {
            return toastr.warning("Please Enter Balloted Count!");
        }

        var reqObj = {
            GiftType: vm.CurrentGiftType,
            GameType: vm.CurrentGameType,
            Location: vm.LocationId,
            StartDate: vm.stDate,
            EndData: vm.enDate,
            SelfieCount: vm.CurrentSelfieCount,
            BallotedCount: vm.BallotedCount,
            ContestantId: data.ContestantId,
            WinningDate: vm.stDate
        };

        if (data.ContestantId && data.ContestantId > 0) {
            dataservice.InsertWinner(reqObj).then(function (data) {
                return toastr.success('Winner Marked Successfully !');
            });
        }
        else {
            return toastr.warning('Contestant Not Available !');
        }
    }

    Init();
}
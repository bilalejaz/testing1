﻿'use strict';

module.exports = controller;
/* @ngInject */
function controller($log, Restangular, dataservice, toastr, $state, $location, $timeout, rabbitmq, $scope, Config, $rootScope) {

    var vm = this;
    vm.currDate = '';
    vm.BroadcastedProgramData = [];
    vm.Loader = true;
    vm.IsIconChecked = false;
    vm.AllSelfieIds = [];
    vm.OgList = [];
    vm.currentIndex = 0;
    vm.AllSelfies = [];
    vm.stDate = '';
    vm.enDate = '';

    function Init() {
        //dataservice.GetAllSelfies().then(function (data) {
        //    if (data && data.length > 0) {
        //        var temp = [];
        //        for (var i = 0; i <= data.length - 1; i++) {
        //            data[i].IsIconChecked = false;
        //            if (i < 50) {
        //                temp.push(data[i]);
        //            }
        //        }
        //        vm.currentIndex = 50;
        //        vm.AllSelfies = temp;
        //        vm.OgList = data;
        //        vm.Loader = false;
        //    }
        //    else {
        //        vm.Loader = false;
        //    }
        //});
    }

    vm.onDateRangeChange = function (startDate, endDate) {
        vm.Date = '';
        var dd = startDate.getDate();
        var mm = startDate.getMonth() + 1;
        var yyyy = startDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.stDate = stdate.toString();

        var edd = endDate.getDate();
        var emm = endDate.getMonth() + 1;
        var eyyyy = endDate.getFullYear();
        var endate = eyyyy + '/' + emm + '/' + edd;
        vm.enDate = endate.toString();

        vm.LoadMoreData(vm.stDate, vm.enDate);
    }

    vm.onScrollWindow = function () {
        //var k = 0;
        //var ptemp = [];
        //for (var i = vm.currentIndex; i <= vm.OgList.length - 1; i++) {
        //    if (k < 50) {
        //        ptemp.push(vm.OgList[i]);
        //    }
        //    else
        //        break;

        //    k = k + 1;
        //}
        //$rootScope.safeApply(function () {
        //    vm.AllSelfies = vm.AllSelfies.concat(ptemp);
        //});
        //vm.currentIndex = vm.currentIndex + k;
    }

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            vm.onScrollWindow();
        }
    });


    vm.AddVerifySelfie = function (data, CurrentIndex) {
        //vm.AllSelfieIds.push(data.id);
        data.ApprovedStatus = 1;
        data.IsIconChecked = true;
    }
    vm.RemoveVerifySelfie = function (data, CurrentIndex) {
        var alldat = [];
        //if (vm.AllSelfieIds.length > 0) {
        //    alldat = _.remove(vm.AllSelfieIds, function (o) { return o.id == data.id; });
        //    vm.AllSelfieIds = alldat;
        //    data.IsIconChecked = false;
        //}
        data.ApprovedStatus = 2;
        data.IsIconChecked = false;
    }
    vm.removeMarkedData = function () {
        for (var k = 0; k < vm.AllSelfieIds.length; k++)
        {
            _.remove(vm.AllSelfies, function (e) {
                return e.id == vm.AllSelfieIds[k];
            });
        }

        //vm.onScrollWindow();
    }
    vm.fillVerifiedSelfiesIds = function ()
    {
        if (vm.AllSelfies && vm.AllSelfies.length > 0)
        {
            for (var k = 0; k < vm.AllSelfies.length; k++)
            {
                //if (vm.AllSelfies[k].IsIconChecked)
                vm.AllSelfieIds.push(vm.AllSelfies[k].id + "," + vm.AllSelfies[k].ApprovedStatus);

            }
        }

    }
    vm.LoadMoreData = function (startDate, endDate) {
        vm.Loader = true;
        vm.AllSelfies = [];
        vm.OgList = [];
        vm.AllSelfieIds = [];

        dataservice.GetAllSelfies(startDate, endDate).then(function (data) {
            if (data && data.length > 0) {
                var temp = [];
                for (var i = 0; i <= data.length - 1; i++) {
                    data[i].ApprovedStatus = 1;
                    data[i].IsIconChecked = true;
                    temp.push(data[i]);
                    //if (i < 50) {
                    //    temp.push(data[i]);
                    //}
                }
                vm.currentIndex = 50;
                vm.AllSelfies = temp;
                vm.OgList = data;
                vm.Loader = false;
            }
            else {
                vm.Loader = false;
            }
        });
    }
    vm.MarkSelfiesVerified = function () {
        vm.fillVerifiedSelfiesIds();
        if (vm.AllSelfieIds.length > 0) {
            dataservice.MarkSelfiesApproval(vm.AllSelfieIds).then(function (data) {
                if (data) {
                    vm.Loader = false;
                    // vm.removeMarkedData();
                    vm.AllSelfieIds = [];
                    toastr.success('Selfie Verified Successfully!');
                    vm.LoadMoreData(vm.stDate, vm.enDate);
                }
                else {
                    vm.Loader = false;
                    toastr.warning('Not Verified Successfully!');
                }
            });
        }
        else {
            toastr.info('Please Mark Selfies!');
        }
    }

    Init();
}
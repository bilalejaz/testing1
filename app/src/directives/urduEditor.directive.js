﻿'use strict';

module.exports = directive;

/* @ngInject */
function directive($timeout, $rootScope) {

    return {
        restrict: 'E',
        template: '\
        <div class="col-lg-12">\
         <div class="input-group">\
        <input type="radio" name="EN" value="EN" ng-model="selectedLanguage"> EN <input type="radio" name="UR" value="UR" ng-model="selectedLanguage"> UR<br>\
      <input max-length="{{maxLength}}" ng-if="!isTextarea && !isLoading" type="text" class="urdulang input-text" ng-model="model" ng-disabled="disabled"/><textarea id="pkg_script" placeholder="{{placeholder}}" max-length="{{maxLength}}" ng-if="isTextarea && !isLoading" type="text" class="urdulang ng-valid ng-touched ng-not-empty ng-dirty input-text" ng-model="model" ng-disabled="disabled"> </textarea>\
    </div>\
    </div>',
        scope: {

            selectedLanguage: '=',
            model: '=',
            onChange: '&',
            maxLength: '=',
            isTextarea: '=',
            disabled: '=',
            placeholder: '@'
        },
        link: function (scope, element, attrs) {
            scope.isLoading = false;
            setTimeout(function () {
                var defaultLanguage = scope.selectedLanguage;
                scope.languages = [{ key: 1, label: "UR" }, { key: 2, label: "EN" }];


                var input = $(element).find('.input-text')[0];
                //$(input).UrduEditor();
                //$(input).addClass('urdulang');
                //$(input).css('font-family', "nafees");
                //$(input).css('fontSize', '125%');
                scope.languageName = defaultLanguage;

                scope.toggleDropdown = function () {
                    scope.showOptions = !scope.showOptions;
                }
                bindKeyPress(input);

                scope.languageChange = function (languageCode) {
                    scope.selectedLanguage = languageCode;
                }
                var languageWorking = function (languageCode) {
                    scope.isLoading = true;
                    scope.languageName = languageCode;
                    scope.showOptions = false;
                    setTimeout(function () {
                        $rootScope.safeApply(function () {
                            scope.isLoading = false;
                            scope.model = '';
                        });
                        input = $(element).find('.input-text')[0];
                        $(input).val('');
                      
                        bindKeyPress(input);
                        if (languageCode == 'UR' && scope.selected != languageCode) {

                            $(input).UrduEditor();
                            $(input).addClass('urdulang');
                            $(input).css('font-family', "nafees");
                            //$(input).css('backgroundColor', 'rgb(245, 245, 245)');
                            $(input).css('fontSize', '125%');
                            $(input).removeAttr('placeholder');
                        } else if (languageCode == 'EN' && scope.selected != languageCode) {
                            //$(input).removeClass('urdulang');
                            //$(input).css('font-family', "");
                            //$(input).removeAttr('urdueditorid');
                            //$(input).removeAttr('dir');
                            ////$(input).css('backgroundColor', '');
                            //$(input).css('fontSize', '');
                        }

                    }, 100);
                }
                if (!scope.selectedLanguage) {
                    scope.selectedLanguage = defaultLanguage;
                }

                scope.$watch('selectedLanguage', function (newVal, oldVal) {
                    if (newVal != oldVal) {
                        languageWorking(scope.selectedLanguage);
                    }
                })

                scope.$watch('model', function (newVal, oldVal) {
                    if (newVal != oldVal) {
                        $(input).val(scope.model);
                    }
                })
            }, 200);

            function bindKeyPress(input) {
                $(input).on('change keyup', _.debounce(function () {
                    if ($(this).val()) {
                        scope.model = $(this).val();
                    }
                }, 300));
            }
        }
    };
}

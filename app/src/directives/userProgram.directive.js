'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice, localStorageService) {
    return {
        link: link,
        restrict: 'E',
        template: require('./userProgram.directive.html'),
        scope: {
            onSelectProgram: '&',
        }
    };


    function link(scope, elem, attrs) {
        var userData = localStorageService.get('userInfo');
        var programsMeta = _.filter(userData.MetaData, function (meta) { return meta.MetaTypeId == 1018 });

        var programsData = [];
        $.each(programsMeta, function (i, v) {
            programsData.push({ id: v.MetaValue, text: v.MetaName, name: v.MetaName, selected: i == 0 ? true : false });
            if (i == 0) {
                scope.onSelectProgram({ 'programId': v.MetaValue, "Name": v.MetaName });
            }
        });

       
        setTimeout(function () {
            var cat = $(elem).find("select");
            cat.select2({
                data: programsData,
                minimumResultsForSearch: Infinity
            });
            cat.on('select2:select', function (evt) {
                scope.onSelectProgram({ 'programId': cat.val(), "Name": evt.params.data.Name });
            });
        }, 300);
    }

}

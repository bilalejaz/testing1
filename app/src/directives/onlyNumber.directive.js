'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice, Restangular) {
    return {
        link: link,
        restrict: 'EA',
        require: 'ngModel'
    };


    function link(scope, elem, attrs, ngModel) {
        scope.$watch(attrs.ngModel, function(newValue, oldValue) {
            var spiltArray = String(newValue).split("");
            if (spiltArray.length === 0) return;
            if (spiltArray.length === 1 
                 && (spiltArray[0] == '-' 
                 || spiltArray[0] === '.' )) return;
            if (spiltArray.length === 2 
                 && newValue === '-.') return;

            /*Check it is number or not.*/
            if (isNaN(newValue)) {
                ngModel.$setViewValue(oldValue);
                ngModel.$render();
            }
        });
    }

}

'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice) {
    return {
        link: link,
        restrict: 'E',
        template: require('./celebrity.directive.html'),
        scope: {
            test: '=',
            onSelectCelebrity: '&',
            clearText: '=',
            celebrityItem: '=',
            addCelebrity: '='
        }
    };


    function link(scope, elem, attrs) {
        var localData = [];
        var cat = $(elem).find("select");
        scope.$watch('model', function (newVal, oldVal) {
            if (newVal != oldVal) {
                console.log(newVal)
            }
        });

        function rebindSelect() {
            var cat = $(elem).find("select");

            cat.select2({
                data: localData,
                tags: true,
                placeholder: "Enter Celebrity...",
                maximumSelectionLength: 20,
                ajax: {
                    url: function (params) {
                        params.getActiveCelebrity = 'false';
                        return "/api/celebrity/getcelebritybyterm/";
                    },
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        $.each(data.Data, function (i, v) { v.id = v.CelebrityId; v.text = v.Name; v.name = v.Name; });
                        localData = data.Data;

                        return {
                            results: data.Data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 3,
                templateResult: function (entity) {
                    if (entity.loading) return entity.text;
                    return entity.name;
                },
                templateSelection: function (entity) {
                    scope.onSelectCelebrity({ 'celebrity': entity });
                    return entity.name || entity.text;
                },
            });
        }

        setTimeout(function () {
            rebindSelect();
        }, 300);

        scope.$watch('addCelebrity', function () {
            localData = [];
            $(cat).find('option').remove()
            if (scope.celebrityItem && scope.celebrityItem.CelebrityId) {
                localData.push({
                    id: scope.celebrityItem.CelebrityId,
                    text: scope.celebrityItem.CelebrityName,
                    name: scope.celebrityItem.CelebrityName,
                    CelebrityId: scope.celebrityItem.CelebrityId,
                    selected: true
                });
                rebindSelect();

            }
        });

        scope.$watch('clearText', function () {
            if (scope.clearText) {
                cat.select2('val', 0);
            }
        });
    }

}
﻿'use strict';

module.exports = directive;

/* @ngInject */
function directive($rootScope, $log, dataservice, $state) {

    return {
        link: link,
        restrict: 'E',
        template: require('./newslist.directive.html'),
        scope: {
            news: '=',
            programnews: '=',
            type: '@',
            onNewsSelect: '&',
            smallView: '@',
            onDeleteProgram: '&',
            showBroadcastCount: '='
        }
    };

    function link(scope, elem, attrs) {
        console.log(scope.showBroadcastCount)
        //console.log(scope.news);
        scope.news.ImageUrl = scope.news.ResourceGuid ? 'http://10.3.12.119/api/resource/getthumb/' + scope.news.ResourceGuid : 'http://10.3.12.118/content/images/producer/noimage-730x380.jpg';
        scope.news.Created = moment(scope.news.CreationDateStr).fromNow();

        for (var i = 0; i <= scope.news.Resources && scope.news.Resources.length - 1; i++) {
            scope.news.Resources[i].ImageUrl = scope.news.Resources[i].Guid ? 'http://10.3.12.119/api/Resource/getthumb/' + scope.news.Resources[i].Guid : 'http://10.3.12.119/content/images/producer/noimage-730x380.jpg';
        }


        if (scope.news.Resources && scope.news.Resources.length >= 5) {
            scope.news.styl = 'display:block';
            scope.news.resCount = scope.news.Resources.length;
        }
        else {
            scope.news.styl = 'display:none';
        }

        scope.selectNews = function (e) {
            scope.onNewsSelect(scope.news);
        }

        scope.deleteProgram = function (e) {
            scope.onDeleteProgram({ newsFileId: scope.news.NewsFileId });
        }

        scope.calculateTooltip = function (data) {
            var tooltip = '';
            if (data.length > 0) {
                $.each(data, function (i, v) {
                    tooltip += v.ProgramName + ' ' + moment(new Date(v.CreationDateStr)).format('lll') + '<br>';
                })

            } else {
                tooltip = 'No data found.';
            }

            scope.tooltipText = tooltip;
        }
        scope.getBroadcastNewsDetail = function (ParentNewsFileId) {
            dataservice.GetBroadcatedNewsFileDetail(ParentNewsFileId).then(function (data) {
                if (data.IsSuccess) {
                    $rootScope.safeApply(function () {
                        scope.calculateTooltip(data.Data);
                    });

                    if ($('.xtooltip').length > 0) {
                        $('.xtooltip').html(scope.tooltipText);
                    }
                }
                else {

                }
            });
        }
    }

}

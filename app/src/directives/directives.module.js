'use strict';

//require('./directives.scss');

var name = module.exports = 'BolSelfie.directives';

angular
  .module(name, [])
  .config(configuration)
  .controller('Directives', require('./directives.controller.js'))
    .directive('bindHtmlCompileValue', require('./bindHtmlCompileValue.directive.js'))
   .directive('fileUpload', require('./fileUpload.directive.js'))
   .directive('userinfo', require('./userinfo.directive.js'))
   .directive('newsEditorBox', require('./newsEditorBox.directive.js'))
   .directive('urduEditor', require('./urduEditor.directive.js'))
    .directive('urduEditorOnly', require('./urduEditorOnly.directive.js'))
    .directive('imageViewer', require('./imageViewer.directive.js'))
   .directive('navbar', require('./navbar.directive.js'))
   .directive('guestDetail', require('./guestDetail.directive.js'))
   .directive('guestList', require('./guestlist.directive.js'))
  .directive('guestListItem', require('./guestListItem.directive.js'))

  .directive('newsCategory', require('./newsCategory.directive.js'))
  .directive('newsLocation', require('./newsLocation.directive.js'))
  .directive('newsTag', require('./newsTag.directive.js'))
  .directive('newsEntity', require('./newsEntity.directive.js'))
  .directive('newsDate', require('./newsDate.directive.js'))
  .directive('userProgram', require('./userProgram.directive.js'))
.directive('celebrity', require('./celebrity.directive.js'))
.directive('dropdown', require('./dropdown.directive.js'))
.directive('singleDate', require('./singleDate.directive.js'))
.directive('panelAddUpdateSegment', require('./panelAddUpdateSegment.directive.js'))
.directive('buttonSwitch', require('./buttonSwitch.directive.js'))
.directive('segmentDate', require('./segmentDate.directive.js'))
.directive('onlyNumber', require('./onlyNumber.directive.js'))

;

function configuration($stateProvider) {
    $stateProvider
      .state('directives', {
          url: '/directives',
          template: require('./directives.html'),
          controller: 'Directives as vm',
          title: 'Welcome to Wayne Minor.'
      });
}





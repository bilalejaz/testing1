'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice, Restangular, $stateParams) {
    return {
        link: link,
        restrict: 'E',
        template: require('./newsCategory.directive.html'),
        scope: {
            test: '=',
            categoryId: '=',
            categoryName: '=',
            onSelectCategory: '&',
            clearText: '='
        }
    };


    function link(scope, elem, attrs) {
        var data = [];



        scope.newsCategory = '';
        var newsId = $stateParams.newsId;
        if (newsId > 0) {
            getNewsCategory(newsId);
        }        

        scope.$watch('model', function (newVal, oldVal) {
            if (newVal != oldVal) {
                console.log(newVal)
            }
        });

        var cat = $(elem).find("select");
        setTimeout(function () {
            if (scope.categoryId) {
                data.push({ id: scope.categoryId, text: scope.categoryName });
            }
            var cat = $(elem).find("select");

            cat.select2({
                data: data,
                tags: false,
                ajax: {
                    url: function (params) {
                        return "/api/category/getcategorybyterm/" + params.term;
                    },
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        $.each(data.Data, function (i, v) { v.id = v.CategoryId; v.text = v.Category; v.name = v.Category; });

                        return {
                            results: data.Data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                templateResult: function (entity) {
                    if (entity.loading) return entity.text;
                    return entity.name;
                },
                templateSelection: function (entity) {
                    return entity.name || entity.text;
                }
            });
            cat.on('select2:select', function (evt) {
                scope.onSelectCategory({ 'categoryId': cat.val()[0] });
            });
            cat.select2("val", scope.categoryName);
        }, 300);

        scope.$watch('clearText', function () {
            if (scope.clearText == true) {
                cat.select2("val", 0);
            }
        });

        function getNewsCategory(newsId) {
            var categories = [];
            var news = getNewsDetails(newsId).then(function (response) {
                for (var i = 0; i < response.Data.Categories.length; i++) {
                    var category = {};
                    category.id = response.Data.Categories[i].CategoryId;
                    category.text = response.Data.Categories[i].Category;
                    category.name = response.Data.Categories[i].Category;
                    categories.push(category);
                }
            });
        }

        function getNewsDetails() {
            var url = 'newsfile/getnews/' + newsId;
            return Restangular.one(url).get();
        }
    }

}

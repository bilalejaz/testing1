'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice, $state, Config, $rootScope) {

    return {
        link: link,
        restrict: 'E',
        template: require('./navbar.directive.html'),
        scope: {
            menuItems: '='
        }
    };

    function link(scope, elem, attrs) {

        scope.navigate = function (item) {
            Config.CurrentBroadcastedEpisodeId = 0;
            var preMenuSelected = _.filter(scope.menuItems, function (m) { return m.Active; })[0];
            var currentMenu = _.filter(scope.menuItems, function (m) { return m.route == item.route; })[0]
            if (preMenuSelected) {
                preMenuSelected.Active = false;
            }
            if (currentMenu) {
                currentMenu.Active = true;
            }
            $state.go(item.route);
        };

        var currentMenu = _.filter(scope.menuItems, function (m) { return m.route == $state.current.name })[0]
        if (currentMenu) {
            currentMenu.Active = true;
        }

        $rootScope.$on('$stateChangeStart', function (next, current) {
            var preMenuSelected = _.filter(scope.menuItems, function (m) { return m.Active; })[0];
            if (preMenuSelected) {
                preMenuSelected.Active = false;
            }
            var currentMenu = _.filter(scope.menuItems, function (m) { return m.route == current.name; })[0]
            currentMenu.Active = true;
            });

    }
}

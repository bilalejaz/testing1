﻿'use strict';

module.exports = directive;

/* @ngInject */
function directive($rootScope) {

    return {
        link: link,
        restrict: 'E',
        template: '<input max-length="1000" ng-if="!isTextarea" type="text" class="urdulang" ng-model="model" ng-disabled="disabled"/><textarea id="pkg_script" placeholder="تبصرہ" max-length="1000" ng-if="isTextarea" type="text" class="urdulang ng-valid ng-touched ng-not-empty ng-dirty" ng-model="model" ng-disabled="disabled"> </textarea>',
        scope: {
            model: '=',
            disabled: '=',
            onChange: '&',
            maxLength: '=',
            isTextarea: '='
        }
    };

    function link(scope, elem, attrs) {
        setTimeout(function () {
            binding(scope, elem, attrs)
        }, 100);

    }

    function binding(scope, elem, attrs) {
        var input = $(elem).find('[type="text"]');
        $(input).UrduEditor();
        $(input).on('change keyup', function () {

            var val = $(input).val();
            if (scope.maxLength && val.length > scope.maxLength) {
                val = val.substr(0, scope.maxLength);
                $(input).val(val);
            }
            setTimeout(function () {
                $rootScope.safeApply(function () {

                    scope.model = val;
                    scope.onChange({
                        val: scope.model
                    });
                });
            }, 100);
        });
        var isCtrlPressed = false;
        $(input).on('keydown', function (e) {
            var key = e.which || e.keyCode;
            var controlKeys = [8, 9, 13];

            controlKeys = controlKeys.concat([37, 38, 39, 40]);
            var isControlKey = event.ctrlKey || controlKeys.join(",").match(new RegExp(key));
            var isPaste = false;
            if (isCtrlPressed && key == 86) {
                isPaste = true;
            }
            if (event.ctrlKey) { isCtrlPressed = true; } else { isCtrlPressed = false; }
            if (isControlKey && !isPaste) { return; }


            if (scope.maxLength && $(input).val().length >= scope.maxLength) {
                e.preventDefault();
                return false;
            }
            return true;
        });
    }


}

'use strict';

module.exports = directive;

/* @ngInject */
function directive($rootScope, dataservice) {
    return {
        link: link,
        restrict: 'E',
        template: require('./singleDate.directive.html'),
        scope: {
            test: '=',
            newsDate: '=',
            timePicker: '=',
            onDateSelect: '&',
            range: '='
        }
    };


    function link(scope, elem, attrs) {
        //debugger
        //for current month
        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 0);

        //for previous month
        var pdate = new Date();
        var py = pdate.getFullYear(), pm = pdate.getMonth() - 1;
        var pfirstDay = new Date(py, pm, 1);
        var plastDay = new Date(py, pm + 1, 0);

        var ranges = {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'This Week': [moment(), moment().add(6, 'days')],
            'Last Week': [moment().subtract(6, 'days'), moment()],
            'This Month': [moment(), moment().add(1, 'months')],
            //'This Month': [firstDay, lastDay],
            'Last Month': [moment().subtract(1, 'months'), moment()]
            //'Last Month': [pfirstDay, plastDay]
        };
        scope.dateSingleTime = {};
        setTimeout(function () {
            $rootScope.safeApply(function () {
                if (scope.range) {
                    scope.dateSingleTime = {
                        startDate: moment(),//moment().subtract(1, "days"),
                        endDate: moment().add(1, 'months')//moment()
                    };
                } else {
                    scope.dateSingleTime = !scope.newsDate || scope.newsDate == "" ? new Date() : new Date(scope.newsDate);
                }
            });
        }, 1000);

        scope.dateSingleTimeOpts = {
            applyClass: '',
            cancelClass: '',
            buttonClasses: '',
            opens: "left",
            timePicker: scope.timePicker,
            singleDatePicker: scope.range ? false : true,
            locale: {
                applyLabel: "Apply",
                fromLabel: "From",
                format: scope.timePicker ? "DD MMM, YYYY h:mm A" : "DD MMM, YYYY",
                toLabel: "To",
                cancelLabel: 'Cancel',
                customRangeLabel: 'Custom range',
            },
            eventHandlers: {
                'show.daterangepicker': function (ev, picker) {
                    console.log('show.daterangepicker', ev, picker);
                },
                'hide.daterangepicker': function (ev, picker) {
                    console.log('hide.daterangepicker', ev, picker);
                },
                'showCalendar.daterangepicker': function (ev, picker) {
                    console.log('showCalendar.daterangepicker', ev, picker);
                },
                'hideCalendar.daterangepicker': function (ev, picker) {
                    console.log('hideCalendar.daterangepicker', ev, picker);
                },
                'apply.daterangepicker': function (ev, picker) {
                    console.log('apply.daterangepicker', ev, picker);
                },
                'cancel.daterangepicker': function (ev, picker) {
                    console.log('cancel.daterangepicker', ev, picker);
                }
            }
        };
        if (scope.range) {
            scope.dateSingleTimeOpts.ranges = ranges;
        }

        scope.$watch('dateSingleTime', function (newVal, oldVal) {
            if (newVal != oldVal) {
                if (scope.range) {
                    scope.onDateSelect({ 'startDate': new Date(newVal.startDate._d.setHours(0, 0, 0, 0)), 'endDate': new Date(newVal.endDate._d.setHours(0, 0, 0, 0)) });
                } else {
                    if (scope.timePicker) {
                        scope.onDateSelect({ 'NewDate': new Date(new Date(newVal)) });
                    }
                    else {
                        scope.onDateSelect({ 'NewDate': new Date(new Date(newVal).setHours(0, 0, 0, 0)) });
                    }
                }
            }
        });
    }

}

'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, userservice) {

    return {
        link: link,
        restrict: 'E',
        template: require('./guestListItem.directive.html'),
        scope: {
            guestsItem: '=',
            type: '@',
        }
    };

    function link(scope, elem, attrs) {
        
    }
}

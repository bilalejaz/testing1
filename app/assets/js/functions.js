$(function(e){
    $("[data-imgsrc]").each(function () {
        $(this).css('background-image', 'url(' + $(this).attr("data-imgsrc") + ')');
    });

    $(".datepicker").datepicker({
        dateFormat: "MM d, y",
        showOtherMonths: true,
    });

    $('.dataList li').on('click',this,function () {
        $('.dataList li').removeClass('active');
        $(this).addClass('active');
        $('.blankScreen ').addClass('dnone');
        $('.detailsSec').removeClass('dnone');
    });

    $('[data-targetit]').on('click',function () {
        $(this).addClass('boxActive');
        $(this).siblings().removeClass('boxActive');
        var target = $(this).data('targetit');
        $('.'+target).siblings('[class^="box-"]').hide();
        $('.'+target).fadeIn();
    });

    //$('.topListBox .openSB').on('click',function () {
    //    $('body').addClass('showSidebar');
    //    $('.overlay').show();
    //});
    $('.overlay').on('click',function () {
        $('body').removeClass('showSidebar');
        $('.overlay').hide();
    });

    //$('.topListBox .closeActi').on('click', function () {
    //    $('body').find('.boxActive').removeClass('boxActive');
    //    $('body').find('.closeActi').addClass('boxActive');
    //});
    //$('.topListBox .checkActi').on('click', function () {
    //    $('body').find('.boxActive').removeClass('boxActive');
    //    $('body').find('.checkActi').addClass('boxActive');
    //});

    setHeight();

    $('p .moreBtn').on('click',function () {
        var more = $(this).html();
        if(more == 'more'){
            $(this).html('less');
            $(this).parent('p').children('.hideTxt').show();
        } else if(more == 'less') {
            $(this).html('more');
            $(this).parent('p').children('.hideTxt').hide();
        }
    });
});

function setHeight(){
    setInterval(function (e) {
        $("[data-scrollheight]").each(function () {
            $this = $(this);
            var ht = '',divList='',setHeight='', curht = 0;
            ht = $(window).outerHeight();
            curht = $this.height();
            divList = $this.attr('data-scrollheight').split(/\s+/);
            $.each(divList, function(index, item) {
                if(item == ''){
                    ht = '100vh';
                    setHeight = ht;
                }else{
                    ht = ht - $('.'+item).eq(0).outerHeight();
                    //$('.'+item).eq(0).css('border','1px solid red');
                    setHeight = ht;
                }
            });

            if(setHeight != curht){
                $this.height(setHeight);
            }
        });
    },300);
}

$(window).on("load",function(){
    setTimeout(function () {
        $(".addScroll").each(function(){
            var $this = $(this),
                scrollInertia = 1000,
                scrollAmount = 200;
            if($this.attr('data-scroll-inertia')){scrollInertia = parseInt($this.attr('data-scroll-inertia'));}
            if($this.attr('data-scroll-amount')){scrollAmount = parseInt($this.attr('data-scroll-amount'));}

            $this.mCustomScrollbar({
                theme: 'minimal',
                axis: 'y', // enable 2 axis scrollbars by default
                scrollInertia: scrollInertia,
                autoHideScrollbar: true,
                advanced: {
                    updateOnContentResize: true
                },
                mouseWheel:{
                    scrollAmount: scrollAmount
                }
            });
        });
    },100);

    $(".addScroll2").each(function(){
        var $this = $(this),
            scrollInertia = 1000,
            scrollAmount = 200;
        if($this.attr('data-scroll-inertia')){scrollInertia = parseInt($this.attr('data-scroll-inertia'));}
        if($this.attr('data-scroll-amount')){scrollAmount = parseInt($this.attr('data-scroll-amount'));}

        $this.mCustomScrollbar({
            theme: 'minimal',
            axis: 'x', // enable 2 axis scrollbars by default
            scrollInertia: scrollInertia,
            autoHideScrollbar: true,
            advanced: {
                updateOnContentResize: true
            },
            mouseWheel:{
                scrollAmount: scrollAmount
            }
        });
    });

    $("[data-height]").each(function () {
        $(this).css('height',$(this).attr("data-height"));
    });
});